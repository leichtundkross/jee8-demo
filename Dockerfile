FROM jboss/wildfly:15.0.0.Final
COPY target/jee8-demo.war /opt/jboss/wildfly/standalone/deployments/
EXPOSE 8080 8080

RUN /opt/jboss/wildfly/bin/add-user.sh jboss jboss --silent
RUN /opt/jboss/wildfly/bin/add-user.sh -a jboss jboss -g guest --silent
CMD /opt/jboss/wildfly/bin/standalone.sh -c standalone-full.xml -b=$(hostname -i) -bmanagement=0.0.0.0