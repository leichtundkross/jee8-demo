package com.github.leichtundkross.jee8.jaxrs;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

// must be a EJB since we use @Asynchronous
@Stateless
@Path("customer")
public class CustomerResource {

	@Context
	private UriInfo uriInfo;

	@PersistenceContext
	private EntityManager em;

	@POST
	@Path("{name}")
	@Produces({ "application/json; qs=0.8" ,"text/plain; qs=0.75" })
	public Response createCustomer(@PathParam("name") String name) {
		final Customer customer = new Customer(name);
		em.persist(customer);
		return Response.ok().entity(customer).build();
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json; qs=0.8" ,"text/plain; qs=0.75" })
	public Response getCustomer(@PathParam("id") long id) {
		final Customer customer = em.find(Customer.class, id);
		return Response.ok().entity(customer).build();
	}

	@GET
	@Path("hateoas/{name}")
	@Produces("application/json")
	public Response getCustomerHateoas(@PathParam("name") String name) {
		return Response.ok() //
				.entity(new Customer(name)) //
				.links(Link.fromUri(uriInfo.getAbsolutePath()).rel("self").type("GET").build(), //
						Link.fromUri(uriInfo.getAbsolutePath() + "/invoice").rel("invoice").type("GET").build()) //
				.build();
	}

	// 1 JAX-RS runtime dispatches a thread to accept a connection
	// 2 The connection is accepted and is handed over to an EJB worker thread for background processing
	// JAX-RS runtime releases the acceptor thread and returns it to the pool. It can then use it to accept more connections
	@GET
	@Path("all")
	@Asynchronous
	@Produces("application/json")
	public void asyncRestMethod(@Suspended final AsyncResponse asyncResponse) {
		asyncResponse.setTimeout(40, TimeUnit.SECONDS);
		asyncResponse.setTimeoutHandler(response -> response.resume(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("TIME OUT!").build()));

		List<Customer> allCustomers = Arrays.asList(new Customer("Google"), new Customer("Facebook"));
		asyncResponse.resume(allCustomers);
	}
}
