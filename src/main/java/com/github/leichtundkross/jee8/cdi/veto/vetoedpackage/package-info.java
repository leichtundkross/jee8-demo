/**
 * Vetoed even works for packages.
 */
@javax.enterprise.inject.Vetoed
package com.github.leichtundkross.jee8.cdi.veto.vetoedpackage;