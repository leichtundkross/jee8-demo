package com.github.leichtundkross.jee8.jms;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.Queue;

@JMSDestinationDefinitions(
      value = {
            @JMSDestinationDefinition(
                  name = "java:/jms/queue/SampleQueue1.1",
                  interfaceName = "javax.jms.Queue",
                  destinationName = "SampleQueue1.1"
            ),
            @JMSDestinationDefinition(
                  name = "java:/jms/queue/SampleQueue2.0",
                  interfaceName = "javax.jms.Queue",
                  destinationName = "SampleQueue2.0"
            ),
      }
)
@Startup
@Singleton
public class JMSInitializer {

   @Resource(lookup = "java:/jms/queue/SampleQueue1.1")
   Queue queue1;

   @Resource(lookup = "java:/jms/queue/SampleQueue2.0")
   Queue queue2;

   @Produces
   public Queue exposeQueue1() {
      return queue1;
   }

   @Produces
   public Queue exposeQueue2() {
      return queue2;
   }
}
