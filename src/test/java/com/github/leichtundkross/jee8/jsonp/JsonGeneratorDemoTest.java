package com.github.leichtundkross.jee8.jsonp;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.StringWriter;
import java.io.Writer;

import org.junit.jupiter.api.Test;


public class JsonGeneratorDemoTest {

	@Test
	public void generateJson() {
		String exptectedJson = "[" //
				+ "{\"type\":\"home\",\"number\":\"(800) 111-1111\"}," //
				+ "{\"type\":\"cell\",\"number\":\"(800) 222-2222\"}]";

		Writer writer = new StringWriter();
		new JsonGeneratorDemo().generateJson(writer);

		assertEquals(exptectedJson, writer.toString());
	}
}
