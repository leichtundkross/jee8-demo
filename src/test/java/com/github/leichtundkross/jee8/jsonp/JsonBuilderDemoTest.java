package com.github.leichtundkross.jee8.jsonp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class JsonBuilderDemoTest {

	@Test
	public void buildJson() {
		String expectedJson = "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"age\":25," //
				+ "\"address\":{\"streetAddress\":\"21 2nd Street\",\"city\":\"New York\",\"state\":\"NY\",\"postalCode\":\"10021\"}," //
				+ "\"phoneNumber\":[{\"type\":\"home\",\"number\":\"212 555-1234\"}," //
				+ "{\"type\":\"office\",\"number\":\"233 555-5678\"}]}";

		String generatedJson = new JsonBuilderDemo().buildJson();

		assertEquals(expectedJson, generatedJson);
	}
}
