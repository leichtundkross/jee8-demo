# jee8-demo

Sample code for certain JavaEE 8 features, including:
* Batch 1.0
* CDI 2.0
* JAX-RS 2.1
* JMS 2.0
* JSON-P 1.1
* WebSocket 1.1
* Concurrency Utilities for Java EE

Project includes a configured wildfly 15 dockerfile to run the sample.